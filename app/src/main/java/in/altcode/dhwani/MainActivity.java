package in.altcode.dhwani;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private static final int REQUEST_PERMISSION = 1122;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ActivityCompat.requestPermissions(this, permissions, REQUEST_PERMISSION);
        mView=findViewById(R.id.message_view);
        sButton=findViewById(R.id.start_button);
    }

    private AudioManager am1;

    private class BecomingNoisyReceiver extends BroadcastReceiver{

        @Override
        public void onReceive(Context context, Intent intent) {
                checkPlayable();
        }
    }

    TextView mView;
    Button sButton;
    BecomingNoisyReceiver myNoisyAudioStreamReceiver;
    private void checkPlayable(){
        if(permissionAccepted) {

            if (am1 == null) {
                am1 = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
                IntentFilter intentFilter = new IntentFilter();
                intentFilter.addAction(AudioManager.ACTION_AUDIO_BECOMING_NOISY);
                intentFilter.addAction(AudioManager.ACTION_HEADSET_PLUG);
                intentFilter.addAction(AudioManager.ACTION_HDMI_AUDIO_PLUG);
                intentFilter.addAction(AudioManager.ACTION_SCO_AUDIO_STATE_UPDATED);
                myNoisyAudioStreamReceiver = new BecomingNoisyReceiver();
                registerReceiver(myNoisyAudioStreamReceiver, intentFilter);
            }

            headsetInserted = (am1.isWiredHeadsetOn() || am1.isBluetoothA2dpOn()) || am1.isBluetoothScoOn();
            if (headsetInserted) {
                sButton.setVisibility(View.VISIBLE);
                mView.setText("");
            }
            else{
                if(audioWorker!=null && audioWorker.isPlaying())stopCast();
                sButton.setVisibility(View.GONE);
                mView.setText("Please connect to a wired or wireless headset/speaker!");
            }
        }
        else {
            sButton.setVisibility(View.GONE);
            mView.setText("Please grant the permissions!");
        }
    }

    private boolean permissionAccepted = false,headsetInserted;

    private String [] permissions = {Manifest.permission.RECORD_AUDIO,Manifest.permission.MODIFY_AUDIO_SETTINGS};


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case REQUEST_PERMISSION:
                permissionAccepted  = grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED;
                checkPlayable();
                break;
        }
        if (!permissionAccepted ) finish();

    }
    AudioWorker audioWorker;
    public void start(View view){
        if(audioWorker!=null && audioWorker.isPlaying()){
            stopCast();
        }
        else {
            startCast();
        }
    }

    private void startCast(){
        audioWorker=new AudioWorker();
        audioWorker.start();
        runOnUiThread(new Runnable(){
            public void run(){
                ((Button)findViewById(R.id.start_button)).setText("Stop");
            }
        });
    }

    private void stopCast(){
        audioWorker.stop();
        runOnUiThread(new Runnable(){
            public void run(){
                ((Button)findViewById(R.id.start_button)).setText("Start");
            }
        });
    }

    @Override
    protected void onDestroy() {
        if(myNoisyAudioStreamReceiver!=null)
            unregisterReceiver(myNoisyAudioStreamReceiver);

        super.onDestroy();
    }
}
