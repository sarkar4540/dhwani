package in.altcode.dhwani;

import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.AudioTrack;
import android.media.MediaRecorder;
import android.media.audiofx.AcousticEchoCanceler;
import android.media.audiofx.LoudnessEnhancer;
import android.media.audiofx.NoiseSuppressor;
import android.os.Build;
import android.os.Process;
import android.util.Log;

public class AudioWorker {
    private AudioRecord audioRecord;
    private AudioTrack audioTrack;
    public void start(){
        playing=true;
        Process.setThreadPriority(Process.THREAD_PRIORITY_URGENT_AUDIO);
        final int bufferSize = AudioRecord.getMinBufferSize(freq,
                AudioFormat.CHANNEL_IN_MONO,
                AudioFormat.ENCODING_PCM_16BIT);


        audioRecord = new AudioRecord(MediaRecorder.AudioSource.MIC, freq,
                AudioFormat.CHANNEL_IN_MONO,
                AudioFormat.ENCODING_PCM_16BIT, bufferSize);

        audioTrack = new AudioTrack(AudioManager.STREAM_MUSIC, freq,
                AudioFormat.CHANNEL_OUT_MONO,
                AudioFormat.ENCODING_PCM_16BIT, bufferSize,
                AudioTrack.MODE_STREAM);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            NoiseSuppressor suppressor=NoiseSuppressor.create(audioTrack.getAudioSessionId());
            AcousticEchoCanceler canceler=AcousticEchoCanceler.create(audioTrack.getAudioSessionId());
            if (suppressor != null) {
                suppressor.setEnabled(true);
            }

            if (canceler != null) {
                canceler.setEnabled(true);
            }
        }



        audioTrack.setPlaybackRate(freq);
        audioRecord.startRecording();
        Log.i("Dhwani Worker", "Audio Recording started");
        audioTrack.play();
        Log.i("Dhwani Worker", "Audio Playing started");
        Thread thread = new Thread(new Runnable() {
            public void run() {
                byte[] buffer = new byte[bufferSize];
                while (isPlaying()) {
                    try {
                        audioRecord.read(buffer, 0, bufferSize);
                        audioTrack.write(buffer, 0, buffer.length);
                    } catch (Throwable t) {
                        Log.e("Error", "Read write failed");
                        t.printStackTrace();
                    }
                }
                audioRecord.stop();
                audioRecord.release();
                Log.i("Dhwani Worker", "Audio Recording Stopped");
                audioTrack.stop();
                audioRecord.release();
                Log.i("Dhwani Worker", "Audio Playing Stopped");
            }
        }, "Dhwani Worker Thread");
        thread.start();
    }


    private int freq = 8000;

    private boolean playing;

    public void stop(){
        playing=false;
    }

    public boolean isPlaying(){
        return playing;
    }

}
